#!/bin/bash
WORKDIR=$(pwd)

if [ ! -z $1 ];then
	cd $WORKDIR/repo/
	mkdir $1
	cp -av *$1.pkg.* $1/
	cd $1
	repo-add onix.db.tar.gz ./*.pkg.*
	cd $WORKDIR
else
	cd $WORKDIR/repo/
	repo-add onix.db.tar.gz ./*.pkg.*
	cd $WORKDIR
fi
