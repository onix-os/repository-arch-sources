#!/bin/bash
WORKDIR=$(pwd)
#packages=$(<packages.list)

if [ ! -d packages ]; then
        git clone git@gitlab.com:onix-os/packages.git $WORKDIR/packages
else
        cd $WORKDIR/packages
        git pull -ff
fi

while read line; do
        cd $WORKDIR/packages/$line
        rm -rf *.pkg.*
	source PKGBUILD
	yay -Syu --noconfirm --needed --asdeps "${makedepends[@]}" "${depends[@]}"
        makepkg -f -s -c --noconfirm
        cd $WORKDIR/
        cp $WORKDIR/packages/$line/*.pkg.* ./repo/
done < $WORKDIR/packages.list
