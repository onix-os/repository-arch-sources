#!/bin/bash
WORKDIR=$(pwd)

cd $WORKDIR

if [ ! -d $WORKDIR/repo ]; then
    mkdir -p $WORKDIR/repo
fi

if [ ! -d $WORKDIR/aur-packages ];then
	mkdir -p $WORKDIR/aur-packages
fi

rm -rf $WORKDIR/repo/*

if [ -d $WORKDIR/packages ];then
	rm -rf $WORKDIR/packages
fi

while read line; do
    cd $WORKDIR/aur-packages/
	PACKAGE=$(echo $line | cut -d'|' -f1)
	if [ -d  $PACKAGE ]; then
		cd $PACKAGE
		rm -rf *.pkg.tar.xz 
	fi
done < $WORKDIR/aur.list

while read line; do
	if [ -d $WORKIR/packages/$line ]; then
        	cd $WORKDIR/packages/$line
        	rm -rf *.pkg.tar.xz
	fi
done < $WORKDIR/packages.list
