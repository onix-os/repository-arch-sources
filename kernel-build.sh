#!/bin/bash
WORKDIR=$(pwd)
cd $WORKDIR/packages/linux-onix
makepkg -f -s --noconfirm
cd $WORKDIR/
cp -av $WORKDIR/packages/linux-onix/*.pkg.* ./repo/
