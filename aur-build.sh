#!/bin/bash
WORKDIR=$(pwd)
#aur=$(<aur.list)
while read line; do
        cd $WORKDIR/aur-packages/
	PACKAGE=$(echo $line | cut -d'|' -f1)
	CHECK=$(echo $line | cut -d'|' -f2)

	if [ ! -d $PACKAGE ];then
		git clone https://aur.archlinux.org/$PACKAGE.git $PACKAGE
	fi

	if [ $CHECK != "local" ];then
		git pull -ff
	fi

	cd $PACKAGE
	source PKGBUILD
	yay -Syu --noconfirm --needed --asdeps "${makedepends[@]}" "${depends[@]}"
	rm -rf *.pkg.*
	makepkg -f -s --noconfirm
	cd $WORKDIR/
	cp $WORKDIR/aur-packages/$PACKAGE/*.pkg.* ./repo/
done < $WORKDIR/aur.list
