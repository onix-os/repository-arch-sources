deps:
	yay -Syu rsync go xmlto python-sphinx base-devel go gnome-shell git qt qtcreator openh264 gperf ninja nasm --noconfirm
	go get -v github.com/seletskiy/go-makepkg
	

prepare:
	/bin/bash ./clean.sh

kernel:
	/bin/bash ./kernel-build.sh

package:
	/bin/bash ./packages-build.sh

aur:
	/bin/bash ./aur-build.sh


sf_sync:
	/bin/bash ./sf-sync.sh

local_sync:
	/bin/bash ./local-sync.sh

git_sync:
	git add -A
	git commit -m "update repos"
	git push
	
sync:
	/bin/bash ./make-repo.sh

x86_64_sync:
	/bin/bash ./make-repo.sh x86_64

i386_sync:
	/bin/bash ./make-repo.sh i386

any_sync:
	/bin/bash ./make-repo.sh any

build: prepare aur package

update_git: git_sync
 
all_sync: sync x86_64_sync i386_sync any_sync git_sync

all: build all_sync
